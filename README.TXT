/* $Id  */

-- SUMMARY --

Supports setting theme based on context.

-- REQUIREMENTS --

None.


-- INSTALLATION --

* Enable module in admin/build/modules


-- CONFIGURATION --

* The module adds a "Theme name" reaction in the Context UI.


-- CONTACT --

Current maintainers:

* Jonathan Yankovich (tronathan) - http://drupal.org/user/361851

This project has been sponsored by:

* BUCKET BRIGADE
  Specialized in consulting and planning of Drupal powered sites and
  value gathering teambuilding activities.